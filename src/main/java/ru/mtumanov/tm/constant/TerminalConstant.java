package ru.mtumanov.tm.constant;

public class TerminalConstant {

    public static final String CMD_HELP = "help";

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

}
